use std::process::Command;
use std::str;
use std::fs;
use std::collections::HashMap;
use serde::{Serialize,Deserialize};
use regex::Regex;
use clap::Parser;
use owo_colors::OwoColorize;
use tera::{Tera, Context};

mod settings;
use settings::{Settings, DriverConfig, ClaimConfig};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Generate a HTML report
    #[arg(long)]
    export_html: bool,
    /// Trigger the webhooks for failed tests
    #[arg(long)]
    trigger_webhooks: bool,
}

#[derive(Debug, Serialize, Deserialize)]
struct Results {
    config: String,
    drivers: Vec<Driver>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Driver {
    name: String,
    website: String,
    version: String,
    claims: Vec<Claim>,
    proof_formats: Vec<ProofFormats>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Claim {
    name: String,
    variant_name: String,
    url: String,
    fingerprint: String,
    positive: bool,
    succeeded: bool,
    status: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct ProofFormats {
    name: String,
    claim: Claim,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let settings = Settings::new().unwrap();

    let raw_config = match fs::read("./config.toml") {
        Ok(c) => String::from_utf8(c).unwrap(),
        Err(_) => format!("")
    };

    let re = Regex::new(r"basic_auth_username = .*").unwrap();
    let raw_config = re.replace_all(&raw_config, "basic_auth_username = CREDENTIALS REDACTED").to_string();

    let re = Regex::new(r"basic_auth_password = .*").unwrap();
    let raw_config = re.replace_all(&raw_config, "basic_auth_password = CREDENTIALS REDACTED").to_string();

    let re = Regex::new(r"args = \[([^\]]+)\]").unwrap();
    let raw_config = re.replace_all(&raw_config, "args = [SECRETS/TOKENS REDACTED]").to_string();

    let re = Regex::new(r"([\r\n]+$)").unwrap();
    let raw_config = re.replace_all(&raw_config, "").to_string();

    let mut results = Results {
        config: raw_config,
        drivers: vec![]
    };

    // For each driver
    for _driver in settings.drivers {
        // Get version
        let output = Command::new(&_driver.path)
            .arg("version")
            .output()
            .expect("failed to execute process");
        let respons = String::from_utf8(output.stdout).unwrap();
        let mut driver = Driver {
            name: _driver.name.clone(),
            website: _driver.website.clone(),
            version: respons.replace("\n", "/").clone(),
            claims: vec![],
            proof_formats: vec![],
        };

        if driver.version.chars().last().unwrap() == "/".chars().last().unwrap() {
            driver.version.pop();
        }

        println!("");
        println!("{} > version", _driver.name.underline());
        println!("  {}", driver.version);

        println!("");
        println!("{} > verify-claim [proof formats]", _driver.name.underline());

        // For each proof format
        match settings.proof_formats {
            Some(ref _proof_formats) => {
                for _proof_format in _proof_formats {
                    // Verify the claim
                    let proof_format = ProofFormats {
                        name: _proof_format.name.clone(),
                        claim: verify_claim(&_driver, &_proof_format.claim)
                    };

                    let outcome = match proof_format.claim.succeeded {
                        true  => format!("{:>9}", proof_format.claim.status.green().bold()),
                        false => format!("{:>9}", proof_format.claim.status.red().bold())
                    };

                    println!("  {} {}", outcome, &proof_format.name);
                    
                    driver.proof_formats.push(proof_format);
                }
            },
            None => println!("  None configured")
        }

        println!("");
        println!("{} > verify-claim [service providers]", _driver.name.underline());

        // For each claim
        match settings.claims {
            Some(ref _claims) => {
                for _claim in _claims {
                    // Verify the claim
                    let claim = verify_claim(&_driver, _claim);
        
                    let variant_name = match &claim.variant_name.len() {
                        0 => format!(""),
                        _ => format!("[{}] ", &claim.variant_name)
                    };
                    let posneg = match _claim.expected_result {
                        true  => "positive",
                        false => "negative"
                    };
                    let outcome = match claim.succeeded {
                        true  => format!("{:>9}", claim.status.green().bold()),
                        false => format!("{:>9}", claim.status.red().bold())
                    };
                    println!("  {} {} {}({})", outcome, &claim.name, variant_name, posneg);

                    // Trigger webhooks
                    if !claim.succeeded && args.trigger_webhooks {
                        match settings.webhooks {
                            Some(ref _webhooks) => {
                                for _webhook in _webhooks {
                                    trigger_webhook(
                                        _webhook,
                                        &_claim.url,
                                        &_claim.name,
                                        &_claim.variant_name,
                                    ).await;
                                }
                            },
                            None => (),
                        }
                    }

                    driver.claims.push(claim);
                }
            },
            None => println!("  None configured")
        }

        results.drivers.push(driver);
    }

    if args.export_html {
        println!("");
        match generate_html(results) {
            Ok(_) => println!("HTML report generated"),
            Err(e) => println!("{} when generating the HTML report: {}", "ERROR".red().bold(), e),
        };
    }
}

fn verify_claim(driver: &DriverConfig, claim: &ClaimConfig) -> Claim {
    let output = Command::new(&driver.path)
                            .arg("verify-claim")
                            .arg(&claim.url)
                            .arg(&claim.fingerprint)
                            .args(&claim.args)
                            .output()
                            .expect("failed to execute process");
    let respons = str::from_utf8(&output.stdout).unwrap();
    let exit_status = output.status;

    let re = Regex::new(r"(.*) (.*) (.*) (true|false)").unwrap();
    let cap = re.captures(respons).unwrap();
    let result = &cap[4] == "true";

    let succeeded = exit_status.success() && result == claim.expected_result;

    let status_string = match exit_status.code() {
        Some(code)  => {
            match code {
                0 => {
                    if result == claim.expected_result {
                        format!("{}", "SUCCEEDED")
                    } else {
                        format!("{}", "INCORRECT")
                    }
                },
                10 => format!("{}", "UNSUPPORTED_COMMAND"),
                11 => format!("{}", "UNSUPPORTED_OPTION"),
                12 => format!("{}", "UNABLE_TO_VERIFY_CLAIM"),
                _ => format!("{}", "UNKNOWN_ERROR"),
            }
        },
        None => format!("{}", "FAILED"),
    };

    Claim {
        name: claim.name.clone(),
        variant_name: claim.variant_name.clone(),
        url: claim.url.clone(),
        fingerprint: claim.fingerprint.clone(),
        positive: claim.expected_result == true,
        succeeded: succeeded,
        status: status_string,
    }
}

fn generate_html(template_data: Results) -> Result<(), Box<dyn std::error::Error>> {
    let tera = match Tera::new("templates/**/*.html") {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };
    let result = tera.render("report.html", &Context::from_serialize(&template_data)?)?;
    fs::write("public/index.html", result)?;

    Ok(())
}

async fn trigger_webhook(webhook: &settings::WebhooksConfig, claim_uri: &String, claim_name: &String, claim_variant_name: &String) -> () {
    let mut params = HashMap::new();
    params.insert("status", "failed");
    params.insert("claim_uri", claim_uri);
    params.insert("claim_name", claim_name);
    params.insert("claim_variant_name", claim_variant_name);
    let mut req = reqwest::Client::new()
        .post(&webhook.url)
        .form(&params);
    
    if webhook.basic_auth_username.is_some() && webhook.basic_auth_password.is_some() {
        req = req.basic_auth(
            webhook.basic_auth_username.clone().unwrap(),
            webhook.basic_auth_password.clone(),
        );
    }
    
    let resp = req.send()
        .await;
    
    match resp {
        Ok(r) => println!("    Webhook triggered (status code: {})", r.status()),
        Err(e) => println!("    Webhook not triggered (error: {:?})", e),
    }
    
}