use config::{ConfigError, Config, File};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub drivers: Vec<DriverConfig>,
    pub claims: Option<Vec<ClaimConfig>>,
    pub proof_formats: Option<Vec<ProofFormatsConfig>>,
    pub webhooks: Option<Vec<WebhooksConfig>>,
}

#[derive(Debug, Deserialize)]
pub struct DriverConfig {
    pub name: String,
    pub website: String,
    pub path: String,
}

#[derive(Debug, Deserialize)]
pub struct ClaimConfig {
    pub name: String,
    pub variant_name: String,
    pub url: String,
    pub fingerprint: String,
    pub args: Vec<String>,
    pub expected_result: bool,
}

#[derive(Debug, Deserialize)]
pub struct ProofFormatsConfig {
    pub name: String,
    pub claim: ClaimConfig,
}

#[derive(Debug, Deserialize)]
pub struct WebhooksConfig {
    pub url: String,
    pub basic_auth_username: Option<String>,
    pub basic_auth_password: Option<String>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            .add_source(File::with_name("config.toml"))
            .build()?;
        s.try_deserialize()
    }
}
