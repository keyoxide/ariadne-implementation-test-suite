# ariadne-implementation-test-suite

Automated report on implementations of the Ariadne Spec

Report publicly available on https://tests.ariadne.id

Inspired by:
- https://tests.sequoia-pgp.org/
- https://datatracker.ietf.org/doc/html/draft-dkg-openpgp-stateless-cli-01

## Usage

### Configuration

Make a copy of the `config.template.toml` file and name it `config.toml`.

At least one `drivers` item is required.

### For simple testing

```
cargo run
```

### For HTML report generation

```
cargo run -- --export-html
```

Exports a HTML report stored in `public/index.html`.

### For webhook triggering

Useful for automated continuous checking of known working identity claims. Will
detect when service providers make changes that interfere with account
verification.

```
cargo run -- --trigger-webhooks
```

Specify at least one `webhooks` item.